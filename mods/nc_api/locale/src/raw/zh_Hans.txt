msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-27 05:07+0200\n"
"PO-Revision-Date: 2022-03-28 10:07+0000\n"
"Last-Translator: Gao Tiesuan <yepifoas@666email.com>\n"
"Language-Team: Chinese (Simplified) <https://hosted.weblate.org/projects/"
"minetest/nodecore/zh_Hans/>\n"
"Language: zh_Hans\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.12-dev\n"

msgid "- @1"
msgstr "- @1"

msgid "- Aux+drop any item to drop everything."
msgstr "- 辅助键+丢弃来丢弃所有物品。"

msgid "- Be wary of dark caves/chasms; you are responsible for getting yourself out."
msgstr "- 注意洞穴和深坑；你自己想办法怎么出来。"

msgid "- Can't dig trees or grass? Search for sticks in the canopy."
msgstr "- 挖不了树和玻璃？在树冠上找找树枝。"

msgid "- Climbing spots may be climbed once black particles appear."
msgstr "- 出现黑色颗粒，就可以爬上攀登点。"

msgid "- Crafting is done by building recipes in-world."
msgstr "- 在世界中建造合成模式来合成物品。"

msgid "- DONE: @1"
msgstr "- 完成: @1"

msgid "- Displaced nodes can be climbed through like climbing spots."
msgstr "- 移位的节点可以像攀登点一样被爬过去。"

msgid "- Do not use F5 debug info; it will mislead you!"
msgstr "- 不要使用F5调试信息，会误导你的！"

msgid "- Drop and pick up items to rearrange your inventory."
msgstr "- 丢弃然后拿起物品来重排你的背包。"

msgid "- For larger recipes, the center item is usually placed last."
msgstr "- 在较大的合成模式中，中间的物品一般最后放置。"

msgid "- "Furnaces" are not a thing; discover smelting with open flames."
msgstr "- 没有“熔炉”这个东西；试着用明火熔炼。"

msgid "(C)2018-2021 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2021 作者 Aaron Suen <warr1024@@gmail.com>"

msgid "- Climbing spots also produce very faint light; raise display gamma to see."
msgstr "- 攀登点也会产生非常微弱的光线；提高显示伽马查看。"

msgid "- Drop items onto ground to create stack nodes. They do not decay."
msgstr "- 将物品扔到地上，形成堆方块。堆方块不会腐烂。"
