-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("api")
include("glass")
include("glued")
include("lens")
include("prism")
include("cooking")
include("crafting")
include("domainwalls")
include("shelf")
include("hints")
